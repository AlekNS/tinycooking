<!DOCTYPE html>
<html lang="en" data-ng-app="tinycooking">
<head>
@include('layouts._header')
</head>
<body>
    <div id="app" data-ng-controller="MainCtrl">
        @yield('content')
    </div>
    <script src="/js/app.js"></script>
</body>
</html>
