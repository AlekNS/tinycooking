@extends('layouts.app')

@section('content')
  <div layout="row" layout-fill>
    <main-menu></main-menu>

    <md-content flex class="tc-content">
      <nav-bar title="headerTitle"></nav-bar>
      <div data-ng-view class="tc-main">
        <h4 class="alert alert-info">Application loading...</h4>
      </div>
    </md-content>
  </div>
@endsection
