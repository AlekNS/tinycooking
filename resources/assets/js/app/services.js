"use strict";

angular.module('tinycooking.services', ['ng', 'ngRoute', 'ngAnimate', 'ngSanitize', 'ngMessages', 'ngResource',
    'ngAria', 'ngMaterial'])
    .service('resources', function ($resource) {
        'ngInject';

        this.recipe = $resource('/api/resources/recipe/:id', {id: '@id'});
        this.ingredient = $resource('/api/resources/ingredient/:id', {id: '@id'});
    })

    .service('storage', function () {

        this.get = function (id) {
            return {
                set: function (objId, obj) {
                    if (objId && obj) {
                        var item = JSON.parse(localStorage.getItem(id) || '{}');
                        item[objId] = obj;
                        localStorage.setItem(id, JSON.stringify(item));
                    }
                    return this;
                },
                unset: function (objId) {
                    var item;
                    if (objId && (item = localStorage.getItem(id))) {
                        item = JSON.parse(item);
                        delete item[objId];
                        localStorage.setItem(id, JSON.stringify(item));
                    }
                    return this;
                },
                get: function (objId) {
                    var item;
                    if (objId && (item = localStorage.getItem(id))) {
                        item = JSON.parse(item);
                        return item[objId];
                    }
                    return null;
                },
                getAll: function () {
                    return JSON.parse(localStorage.getItem(id) || '{}');
                },
                clear: function () {
                    localStorage.removeItem(id);
                    return this;
                }
            };
        }

    })

    .service('commonDialogs', function ($mdDialog) {
        this.confirm = function (message) {
            return $mdDialog.show(
                $mdDialog.confirm()
                    .escapeToClose(true)
                    .clickOutsideToClose(true)
                    .textContent(message)
                    .ariaLabel('Confirm Dialog')
                    .ok('Ok')
                    .cancel('Cancel')
            );
        }
    })

    .service('statDialogs', function ($mdDialog) {
        this.showAggRecipes = function (items) {
            return $mdDialog.show({
                fullscreen: true,
                templateUrl: 'views/directives/stats/dialogs/summaryIngredients.html',
                controller: ($scope, $mdDialog, recipes) => {
                    'ngInject';

                    $scope.recipes = _.cloneDeep(recipes || {});
                    $scope.ingredients = _($scope.recipes)
                        .map(r=>r.ingredients)
                        .flatten()
                        .uniq()
                        .sort()
                        .value();

                    $scope.onClickCancel = () => {
                        $mdDialog.cancel();
                    };

                    $scope.onClickOk = () => {
                        $mdDialog.hide();
                    };
                },
                controllerAs: '$ctrl',
                parent: angular.element(document.body),
                locals: {
                    recipes: items
                },
                bindToController: true,
                clickOutsideToClose: true
            });
        }
    })
    .service('recipeDialogs', function ($mdDialog) {
        'ngInject';

        this.showViewer = function (item) {
            return $mdDialog.show({
                fullscreen: true,
                templateUrl: 'views/directives/recipes/dialogs/view.html',
                controller: ($scope, $mdDialog, recipe) => {
                    'ngInject';

                    $scope.recipe = _.cloneDeep(recipe || {});
                    $scope.recipe.instruction = $scope.recipe.instruction.replace(/\n/, '<br>');

                    $scope.onClickCancel = () => {
                        $mdDialog.cancel();
                    };

                    $scope.onClickOk = () => {
                        $mdDialog.hide();
                    };
                },
                controllerAs: '$ctrl',
                parent: angular.element(document.body),
                locals: {
                    recipe: item
                },
                bindToController: true,
                clickOutsideToClose: true
            });
        };

        this.showEditor = function (item) {
            return $mdDialog.show({
                fullscreen: true,
                templateUrl: 'views/directives/recipes/dialogs/editor.html',
                controller: ($scope, $mdDialog, recipe) => {
                    'ngInject';

                    $scope.recipe = _.cloneDeep(recipe || {});
                    $scope.recipe.ingredients = $scope.recipe.ingredients || [];

                    $scope.headerTitle = $scope.recipe.id ? 'Update Recipe' : 'New Recipe';

                    $scope.onClickCancel = () => {
                        $scope.recipe = {ingredients: []};
                        $mdDialog.cancel();
                    };

                    $scope.onClickOk = (isFormValid) => {
                        if (!isFormValid || !$scope.recipe.ingredients.length) {
                            return;
                        }
                        $mdDialog.hide($scope.recipe);
                    };
                },
                controllerAs: '$ctrl',
                parent: angular.element(document.body),
                locals: {
                    recipe: item
                },
                bindToController: true,
                clickOutsideToClose: true
            });
        }
    })
;