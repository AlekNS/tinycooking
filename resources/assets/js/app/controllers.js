'use strict';

angular.module('tinycooking.controllers', ['tinycooking.services'])
    .controller('MainCtrl', function ($scope) {
        'ngInject';

        $scope.setHeaderTitle = function (header) {
            $scope.headerTitle = header;
        }
    })

    .controller('RecipesCtrl', function ($scope, resources, statDialogs, recipeDialogs, commonDialogs, storage) {
        'ngInject';

        var selectedRecipes = storage.get('selectedRecipes');

        $scope.setHeaderTitle('Recipes');

        function queryRecipes(filters) {
            resources.recipe.query(filters && {filter_name: filters.name}).$promise.then((recipes) => {
                $scope.recipes = recipes;
                $scope.recipes.forEach((r) => {
                    r.selected = !!selectedRecipes.get(r.id);
                });
            });
            $scope.selectedRecipesCount = Object.keys(selectedRecipes.getAll()).length;
        }

        queryRecipes();

        $scope.onSearch = function (filters) {
            queryRecipes(filters);
        };
        $scope.onUnselectAll = function () {
            selectedRecipes.clear();
            $scope.recipes.forEach(r=>r.selected = false);
            $scope.$applyAsync('selectedRecipesCount = 0');
        };
        $scope.onRecipeSelect = function (recipe) {
            selectedRecipes[recipe.selected ? 'set' : 'unset'](recipe.id, _.pick(_.cloneDeep(recipe), ['ingredients']));
            $scope.selectedRecipesCount = Object.keys(selectedRecipes.getAll()).length;
        };
        $scope.onRecipeAdd = function () {
            recipeDialogs.showEditor().then((r) => {
                return resources.recipe.save(r).$promise;
            })
                .then(()=>$scope.onSearch());
        };
        $scope.onRecipeView = function (recipe) {
            recipeDialogs.showViewer(recipe);
        };
        $scope.onRecipeEdit = function (recipe) {
            recipeDialogs.showEditor(recipe).then((r) => {
                return resources.recipe.update(r).$promise;
            })
                .then(()=>$scope.onSearch());
        };
        $scope.onRecipeDelete = function (recipe) {
            commonDialogs.confirm(`Delete ${recipe.name}?`).then(() => {
                return resources.recipe.delete(recipe).$promise;
            })
                .then(()=>{
                    selectedRecipes.unset(recipe.id);
                    $scope.onSearch();
                });
        };
        $scope.onShowAggIngredients = function () {
            statDialogs.showAggRecipes(_.values(selectedRecipes.getAll()));
        };
    })
;
