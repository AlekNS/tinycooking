'use strict';

angular.module('tinycooking.directives', ['tinycooking.services'])
    .component('navBar', {
        templateUrl: 'views/directives/common/navbar.html',
        bindings: {
            title: '<'
        },
        controller: function ($mdSidenav) {
            'ngInject';

            this.toggleLeftMenu = function () {
                angular.element(document.getElementsByClassName('tc-left-sidenav-main')[0]).removeClass('tc-left-sidenav-main-min');
                $mdSidenav('mainMenu').toggle();
            }
        }
    })

    .component('progressBar', {
        template: '<div ng-style="!$ctrl.inProgress && {\'height\':\'5px\'}">' +
                '<md-progress-linear ng-if="$ctrl.inProgress" flex md-mode="query"></md-progress-linear>' +
            '</div>',
        bindings: {
            inProgress: '<'
        },
        controller: function ($scope) {
            'ngInject';
            var debounceEndProgress = null;

            $scope.$on('$httpRequest:api', function (event) {
                if (debounceEndProgress) {
                    return;
                }
                $scope.$applyAsync('$ctrl.inProgress = 1');
                debounceEndProgress = _.debounce(function () {
                    $scope.$applyAsync('$ctrl.inProgress = 0');
                    debounceEndProgress = null;
                }, 400);
            });
            $scope.$on('$httpResponse:api', function (event) {
                debounceEndProgress && debounceEndProgress();
            });
        }
    })

    .component('mainMenu', {
        templateUrl: 'views/directives/common/mainMenu.html',
        controller: function ($mdMedia, $mdSidenav) {
            'ngInject';

            this.$mdMedia = $mdMedia;

            this.openSubmenu = function (event) {
                var curElem = angular.element(event.currentTarget),
                    submenu = curElem.next();

                if (submenu.hasClass('submenu')) {
                    curElem.toggleClass('open');
                    submenu.toggleClass('open');
                }
            };

            this.accordionMenu = function () {
                angular.element(document.getElementsByClassName('tc-left-sidenav-main')[0]).toggleClass('tc-left-sidenav-main-min');
            }
        }
    })

    .component('recipesList', {
        templateUrl: 'views/directives/recipes/list.html',
        bindings: {
            recipes: '=',
            onSelect: '&',
            onView: '&',
            onEdit: '&',
            onDelete: '&'
        },
        controller: function () {
        }
    })

    .component('recipesSearch', {
        templateUrl: 'views/directives/recipes/search.html',
        bindings: {
            onSearch: '&'
        },
        controller: function () {
            this.filters = {};
            this.toggleSearchPanel = function () {
                this.showPanel = !this.showPanel;
            };
            this.search = function () {
                this.onSearch({filters: this.filters});
            }
        }
    })

    .component('recipesActions', {
        templateUrl: 'views/directives/recipes/actions.html',
        bindings: {
            onSearch: '&',
            onAction: '&'
        },
        controller: function () {
        }
    })

    .component('recipesSelection', {
        templateUrl: 'views/directives/recipes/selection.html',
        bindings: {
            selectedRecipesCount: '<',
            onUnselectAll: '&',
            onShow: '&'
        }
    })

;
