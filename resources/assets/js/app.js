'use strict';

require('./bootstrap');

require('./app/services');
require('./app/directives');
require('./app/controllers');

angular.module('tinycooking', ['tinycooking.controllers', 'tinycooking.directives'])

    .config(function ($routeProvider, $httpProvider, $logProvider, $resourceProvider, $mdIconProvider, $provide) {
        'ngInject';

        $logProvider.debugEnabled(true);

        $mdIconProvider.defaultFontSet('fontawesome');

        $routeProvider
            .when('/', {
                controller: 'RecipesCtrl',
                templateUrl: 'views/recipes.html'
            })
            .otherwise('/');

        $provide.factory('defaultHttpInterceptor', function ($q, $rootScope) {
            'ngInject';

            return {
                request: function (config) {
                    if (config.url.startsWith('/api/resources/')) {
                        $rootScope.$broadcast('$httpRequest:api');
                    }
                    return config;
                },
                response: function (response) {
                    if (response.config.url.startsWith('/api/resources/')) {
                        $rootScope.$broadcast('$httpResponse:api');
                    }
                    return response;
                },
                requestError: function (rejection) {
                    $rootScope.$broadcast('$httpRequestError', rejection);
                    return $q.reject(rejection);
                },
                responseError: function (rejection) {
                    $rootScope.$broadcast('$httpResponseError', rejection);
                    return $q.reject(rejection);
                }
            };
        });

        angular.extend($httpProvider.defaults.headers.common, {
            'X-XSRF-TOKEN': window.Laravel.csrfToken
        });

        angular.extend($resourceProvider.defaults.actions, {
            update: {method: 'PUT'}
        });

        $httpProvider.interceptors.push('defaultHttpInterceptor');
    })

    .run(function ($rootScope, $window) {
        'ngInject';

        $rootScope.csrfToken = function () {
            return $window.Laravel.csrfToken;
        };
    })
;
