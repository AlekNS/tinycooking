#!/usr/bin/env bash

echo "Installing Docker"

apt-get install apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo 'deb https://apt.dockerproject.org/repo debian-jessie main' > /etc/apt/sources.list.d/docker.list
apt-get install -y apt-transport-https
apt-get update
apt-get install -y docker-engine pixz

echo "Installing Docker Compose"

curl -L https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

usermod -a -G docker vagrant


echo "Installing nodejs"
mkdir /usr/local/nodejs
cd /usr/local/nodejs
curl https://nodejs.org/dist/v6.9.1/node-v6.9.1-linux-x64.tar.xz | xz -dc - | tar xv
mv node-v6.9.1-linux-x64/* .
ln -s /usr/local/nodejs/bin/node /usr/local/bin/node
ln -s /usr/local/nodejs/bin/npm /usr/local/bin/npm

cd /vagrant
cp -f .env.dev .env
chmod -R ogu+rw ./bootstrap/cache ./storage/framework/{,cache,sessions,views} ./storage/logs

cd /vagrant

echo "NPM install packages"
npm install

echo "Build Frontend"
./node_modules/.bin/gulp
cd /vagrant/docker

echo "Start docker compose"
docker-compose up -d

echo "Ready"
