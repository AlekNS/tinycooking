#!/usr/bin/env bash

echo "Composer install..."
composer install

echo "DB Migration..."
php artisan migrate

echo "DB Seed..."
php artisan db:seed
