Tiny Cooking
==============

Welcome to Tiny Cooking (Laravel, AngularJS, Angular Material).


## Install

1. Download and Install [Vagrant](https://www.vagrantup.com/downloads.html) ([VirtaulBox](https://www.virtualbox.org/wiki/Downloads) installation required!)
1. `$ vagrant up`
1. `$ vagrant ssh -c 'docker exec -ti tinycooking_nginx_app bash ./provision/last_part.bash'`


## Run project

1. `$ vagrant ssh -c 'cd /vagrant/docker; docker-compose up -d'`
1. `$ vagrant ssh -c 'docker exec -ti tinycooking_nginx_app php artisan service:runexrate 10'`
1. Go to url: http://192.168.10.112:8080


## Tests

1. `$ vagrant ssh -c 'docker exec -ti tinycooking_nginx_app php ./vendor/bin/phpunit'`