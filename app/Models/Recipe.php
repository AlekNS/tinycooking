<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'recipes';

    protected $fillable = [
        'name', 'instruction',
    ];

    public $timestamps = false;

    public function ingredients()
    {
        return $this->belongsToMany('App\Models\Ingredient', 'recipe_ingredients', 'recipe_id');
    }

    public function toArray()
    {
        $data = parent::toArray();
        if (!empty($data['ingredients'])) {
            $data['ingredients'] = array_map(function ($i) {
                return $i['name'];
            }, $data['ingredients']);
        }
        return $data;
    }

}
