<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application site.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site');
    }
}
