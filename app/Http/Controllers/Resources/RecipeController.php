<?php namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Http\Request;

// @TODO: Add validations
class RecipeController extends Controller
{
    public function index(Request $request)
    {
        return Recipe::with('ingredients')->where('name', 'like', $request->get('filter_name') . '%')
            ->limit(20)
            ->orderBy('name')
            ->get()
        ->map(function ($r) {
            $r['image_path'] = '/images/sup.png';
            return $r;
        })->values();
    }

    public function show(Recipe $recipe)
    {
        return $recipe->load('ingredients')->toArray();
    }

    protected function syncIngredients(Recipe $recipe, Request $request)
    {
        $ingredientNames = $request->get('ingredients');
        $existingIngredients = Ingredient::whereIn('name', $ingredientNames)->get()->keyBy('name');
        $notExistingIngredients = collect($ingredientNames)->filter(function ($name) use (&$existingIngredients) {
            return !isset($existingIngredients[$name]);
        });
        $notExistingIngredients = $notExistingIngredients->map(function ($name) {
            return Ingredient::create([
                'name' => $name
            ]);
        });

        $recipe->ingredients()->sync(
            collect([$notExistingIngredients->pluck('id'), collect($existingIngredients)->pluck('id')])
            ->flatten()->values()->toArray()
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'instruction' => 'required|string',
            'ingredients' => 'sometimes|array'
        ]);
        $recipe = Recipe::create($request->all());
        $this->syncIngredients($recipe, $request);
        return $recipe->toArray();
    }

    public function update(Recipe $recipe, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'instruction' => 'required|string',
            'ingredients' => 'sometimes|array'
        ]);
        $recipe->fill($request->all());
        $recipe->save();
        $this->syncIngredients($recipe, $request);
        return $recipe->toArray();
    }

    public function destroy(Recipe $recipe)
    {
        $recipe->ingredients()->delete();
        $recipe->delete();
    }
}