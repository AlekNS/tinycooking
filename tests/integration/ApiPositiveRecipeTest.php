<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Auth\User;

class ApiPositiveRecipeTest extends TestCase
{
    protected static $recipeId;

    public function testShouldCreateRecipe()
    {
        $response = $this->postJson('/api/resources/recipe', [
            'name' => 'recipe1',
            'instruction' => 'instruction',
            'ingredients' => ['i1', 'i2']
        ]);

        $response->seeStatusCode(200);

        self::$recipeId = json_decode($response->response->content());
        self::$recipeId = self::$recipeId->id;
    }

    /**
     * @depends testShouldCreateRecipe
     */
    public function testShouldSeeRecipe()
    {
        $this->get('/api/resources/recipe')
            ->seeStatusCode(200)
            ->see('"id":'.self::$recipeId.'');
    }

    /**
     * @depends testShouldSeeRecipe
     */
    public function testShouldRemoveRecipe()
    {
        $this->deleteJson('/api/resources/recipe/'.self::$recipeId)
            ->seeStatusCode(200);
    }
}
