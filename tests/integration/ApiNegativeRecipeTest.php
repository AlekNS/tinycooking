<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Auth\User;

class ApiNegativeRecipeTest extends TestCase
{
    public function testShouldNotCreateRecipeWithoutName()
    {
        $response = $this->postJson('/api/resources/recipe', [
            'instruction' => 'instruction',
            'ingredients' => ['i1', 'i2']
        ]);

        $response->seeStatusCode(500);
    }
}
